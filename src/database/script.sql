-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        philippe
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-13 09:53
-- Created:       2021-12-13 09:53
PRAGMA foreign_keys = OFF;

-- Schema: mydb

BEGIN;
CREATE TABLE "articles"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title" VARCHAR(45) NOT NULL,
  "content" VARCHAR(45) NOT NULL
);
COMMIT;
