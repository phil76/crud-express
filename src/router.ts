import { Application } from "express";
import ArticleController from "./controllers/ArticleController";
import HomeController from "./controllers/HomeController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.index(req, res);
    });

    app.get('/article-all', (req, res) =>
    {
        ArticleController.index(req, res)
    });

    app.get('/article-create', (req, res) =>
    {
        ArticleController.showForm(req, res)
    });

    app.post('/article-create', (req, res) =>
    {
        ArticleController.create(req, res)
    });

    app.get('/article-read/:id', (req, res) =>
    {
        ArticleController.read(req, res)
    });

    app.get('/article-update/:id', (req, res) =>
    {
        ArticleController.showFormUpdate(req, res)
    });

    app.post('/article-update/:id', (req, res) =>
    {
        ArticleController.update(req, res)
    });

    app.get('/article-delete/:id', (req, res) =>
    {
        ArticleController.delete(req, res)
    });
}
