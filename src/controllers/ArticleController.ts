import { Request, Response } from "express-serve-static-core";

export default class ArticleController
{
    /**
     * Affiche la liste des articles
     * @param req 
     * @param res 
     */
    static index(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const articles = db.prepare('SELECT * FROM articles').all();

        res.render('pages/index', {
            title: 'Articles',
            articles: articles
        });
    }

    /**
     * Affiche le formulaire de creation d'article
     * @param req 
     * @param res 
     */
    static showForm(req: Request, res: Response): void
    {
        res.render('pages/article-create');
    }

    /**
     * Recupere le formulaire et insere l'article en db
     * @param req 
     * @param res 
     */
    static create(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        db.prepare('INSERT INTO articles ("title", "content") VALUES (?, ?)').run(req.body.title, req.body.content);

        ArticleController.index(req, res);
    }

    /**
     * Affiche 1 article
     * @param req 
     * @param res 
     */
    static read(req: Request, res: Response): void
    {
        const db = req.app.locals.db;

        const article = db.prepare('SELECT * FROM articles WHERE id = ?').get(req.params.id);

        res.render('pages/article', {
            article: article
        });
    }

    /**
     * Affiche le formulaire pour modifier un article
     * @param req 
     * @param res 
     */
    static showFormUpdate(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        const article = db.prepare('SELECT * FROM articles WHERE id = ?').get(req.params.id);

        res.render('pages/article-update', {
            article: article
        });
    }

    /**
     * Recupere le formulaire de l'article modifié et l'ajoute a la database
     * @param req 
     * @param res 
     */
    static update(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('UPDATE articles SET title = ?, content = ? WHERE id = ?').run(req.body.title, req.body.content, req.params.id);

        ArticleController.index(req, res);
    }

    /**
     * Supprime un article
     * @param req 
     * @param res 
     */
    static delete(req: Request, res: Response)
    {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM articles WHERE id = ?').run(req.params.id);

        ArticleController.index(req, res);
    }
}